import React from 'react';

import { StyleSheet, Text, View, Button, TextInput, Container } from 'react-native';

class Home extends React.Component {

  state = {
    newText: "",
    ToDo: ["arindamdas@gmail.com", "anantadas@yahoo.com"]
  }

  submit = () => {
    var listContent = this.state.newText;
    var arr = this.state.ToDo;

    arr.push(listContent);

    this.setState({ newText: arr, newText: '' });
  }

  Delete = (data) => {
    var arr = this.state.ToDo;
    var pos = arr.indexOf(data);

    arr.splice(pos, 1);

    this.setState({ ToDo: arr });
  }
  renderList = () => {
    return this.state.ToDo.map(result => {
      return (
        <Text style={styles.textStyle} onPress={() => { this.Delete(result) }} >{result}</Text>
      )
    })
  }
  render() {
    return (
      <View style={styles.wholeStyle}>
        <View style={styles.viewStyle} >
          <h3 style={styles.Headers}>Note App</h3>
          <br />

          <TextInput placeholder="Enter Your Email" style={styles.inputField} onChangeText={(text) => this.setState({ newText: text })} />
          <br />


          <Button title="submit" color="#9c27b0" paddingTop="3px" style={styles.buttonStyle} onPress={this.submit} />
          <br />
          {this.renderList()}
        </View>
      </View>
    )
  }
}

const styles = {
  wholeStyle: {
    backgroundColor: '#ce93d8',
    flex: 1
  },
  viewStyle: {
    justifyContent: 'center', alignItems: 'center', margin: 20
  },
  inputField: {
    height: 40,
    paddingLeft: 6,
    marginBottom: 10,
    borderColor: 'green',
    borderWidth: 2,
    width: 400,
    backgroundColor: '#fffde7'

  },
  buttonStyle: {
    height: 30,
    paddingTop: 2,
    marginTop: 3

  },
  textStyle: {
    padding: 6,
    color: '#fffde7',
    fontFamily: 'italic',
    fontWeight: 'bold',
    fontSize: 30,
    marginTop: 6
  },
  Headers: {
    fontSize: 40,
    color: '#fffde7',
    fontWeight: 'bold'
  }
}

export default Home;